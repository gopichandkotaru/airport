import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class App extends Component {
  state = {
    data: ''
  }

  componentDidMount = () => {
    fetch('https://raw.githubusercontent.com/mwgg/Airports/master/airports.json', {
      method: 'GET'
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        this.setState({
          data: responseJson
        })
      })
      .catch((error) => {
        console.error(error);
      });
  }
  render() {
    return (
      <View style={{ borderWidth: 1, borderColor: 'grey', margin: 10, padding: 10 ,borderRadius:20}}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between',marginBottom:15 }}>
          <Text style={{fontSize:20,color:'orange'}}> {this.state.data.name} </Text>
          <Text style={{fontSize:20,color:'orange'}}> {this.state.data.icao} </Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{fontSize:18}}> {this.state.data.city} </Text>
          <Text style={{fontSize:18}}> {this.state.data.state} </Text>
          <Text style={{fontSize:18}}> {this.state.data.country} </Text>

        </View>
      </View>
    );
  }
}
